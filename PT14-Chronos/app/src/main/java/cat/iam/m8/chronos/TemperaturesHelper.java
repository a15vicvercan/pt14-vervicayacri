package cat.iam.m8.chronos;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/*
* Clase encargada de hacer escrituras y lecturas en BD
* */
public class TemperaturesHelper {

    //Lista de Bloques que se ha de guardar en BD
    private ArrayList<Bloc> bloquesTiempo;

    /*
    * Creadoras
    * */
    public TemperaturesHelper() {}
    public TemperaturesHelper (ArrayList bloquesTiempo) {
        this.bloquesTiempo = bloquesTiempo;
    }

    /*
    * Función que guarda en BD la lista de bloques
    * */
    public void guardarBBDD(String ciudad) {
        //Conexión con la BD y escritura en ella de los datos
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(ciudad);
        myRef.setValue(this.bloquesTiempo);
    }

    /*
    * Función que se encarga de leer en BD los datos de una ciudad en concreto
    * */
    public void lecturaDatosBD(final String nombreCiudad, final MyCallBack myCallBack) {
        //Conexion a BD y "consulta" de la ciudad que se le pasa por parámetro
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(nombreCiudad);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<ArrayList<Bloc>> t = new GenericTypeIndicator<ArrayList<Bloc>>() {};
                ArrayList<Bloc> value = dataSnapshot.getValue(t);
                myCallBack.onCallBack(value, nombreCiudad);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("ERROR", "Failed to read value.", error.toException());
            }
        });
    }
}
