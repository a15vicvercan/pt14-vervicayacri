package cat.iam.m8.chronos;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CiutatAdapter extends RecyclerView.Adapter<CiutatAdapter.BlocViewHolder> {

    //Atributo que contendrá los datos necesarios para cargarlos en el RecylcerView
    private ArrayList<Bloc> data;

    public CiutatAdapter(ArrayList<Bloc> data) {
        this.data = data;
    }

    @Override
    public BlocViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bloc, parent, false);
        return new BlocViewHolder(row);
    }

    @Override
    public void onBindViewHolder(BlocViewHolder holder, int position) {
        Bloc bloque = data.get(position);

        //Insertamos los datos para que se muestren por pantalla
        holder.getTxtView_temperatura().setText(String.valueOf(bloque.getTemperatura() + "ºC"));
        holder.getTxtView_dia().setText(String.valueOf(bloque.getDia()));
        holder.getTxtView_hora().setText(bloque.getHora());

        //Segun lo que obtengamos del campo meteo, indicaremos con una imagen su valor
        String tiempo = bloque.getMeteo();
        switch (tiempo) {
            case "Rain":
                holder.getImg_imagen().setImageResource(R.drawable.lluvia);
                break;

            case "Clouds":
                holder.getImg_imagen().setImageResource(R.drawable.nube);
                break;

            case "Clear":
                holder.getImg_imagen().setImageResource(R.drawable.sol);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class BlocViewHolder extends RecyclerView.ViewHolder {

        private TextView txtView_temperatura;
        private TextView txtView_dia;
        private TextView txtView_hora;
        private ImageView img_imagen;

        public BlocViewHolder(View itemView) {
            super(itemView);

            this.txtView_temperatura = itemView.findViewById(R.id.txtView_temperatura);
            this.txtView_dia = itemView.findViewById(R.id.txtView_dia);
            this.txtView_hora = itemView.findViewById(R.id.txtView_hora);
            this.img_imagen = itemView.findViewById(R.id.img_imagen);
        }

        public TextView getTxtView_temperatura() {
            return txtView_temperatura;
        }

        public TextView getTxtView_dia() {
            return txtView_dia;
        }

        public TextView getTxtView_hora() {
            return txtView_hora;
        }

        public ImageView getImg_imagen() {
            return img_imagen;
        }

    }

    public void add(int position, Bloc item) {
        data.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

}