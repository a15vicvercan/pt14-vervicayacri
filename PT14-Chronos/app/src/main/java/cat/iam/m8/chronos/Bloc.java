package cat.iam.m8.chronos;

/*
* Clase que representa cada bloque de 3 horas
* En este bloque se contienen los siguientes datos:
*   - Dia
*   - Hora
*   - Tiempo meteorologico (nublado, lluvia, etc)
*   - Temperatura de ese bloque
* */
public class Bloc {

    String dia, hora, meteo;
    Double temperatura;

    /*
    * Constructoras
    * */
    public Bloc () {

    }

    public Bloc (String dia, String hora, String meteo, Double temperatura) {
        this.dia = dia;
        this.hora = hora;
        this.meteo = meteo;
        this.temperatura = temperatura;
    }

    /*
    * Getters
    * */

    public Double getTemperatura() {
        return temperatura;
    }

    public String getMeteo() {
        return meteo;
    }

    public String getDia() {
        return dia;
    }

    public String getHora() {
        return hora;
    }

    /*
     * Setters
     * */
    public void setMeteo(String meteo) {
        this.meteo = meteo;
    }

    public void setTemperatura(Double temperatura) {
        this.temperatura = temperatura;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}
