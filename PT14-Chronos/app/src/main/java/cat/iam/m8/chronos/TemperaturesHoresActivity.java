package cat.iam.m8.chronos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

public class TemperaturesHoresActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperatures_hores);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        String nombreCiudad = intent.getStringExtra("nombreCiudad");

        TemperaturesHelper temp = new TemperaturesHelper();
        temp.lecturaDatosBD(nombreCiudad, new MyCallBack() {
            @Override
            public void onCallBack(ArrayList<Bloc> value, String nombreCiudad) {
                RecyclerView recyclerView = findViewById(R.id.recyclerView_blocs);
                recyclerView.setHasFixedSize(true);

                layoutManager = new LinearLayoutManager(TemperaturesHoresActivity.this);
                recyclerView.setLayoutManager(layoutManager);

                mAdapter = new CiutatAdapter(value);
                recyclerView.setAdapter(mAdapter);
            }
        });
    }
}
