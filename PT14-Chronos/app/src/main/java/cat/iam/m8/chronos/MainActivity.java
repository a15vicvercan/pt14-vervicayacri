package cat.iam.m8.chronos;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity implements MyCallBack{

    private JSONArray bloquesHoras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /*
     * Función que nos sirve para poder usar los datos de la lectura de la BD en el main
     * */
    @Override
    public void onCallBack(ArrayList<Bloc> value, String nombreCiudad) {

    }

    public void sincronitzarDades(View view) {

        //Obtenemos el nombre de la ciudad que introduce el usuario por pantalla
        EditText city = findViewById(R.id.etCiutat);
        String ciudad = city.getText().toString();

        //Hacemos una primera consulta en BD para ver si la ciudad existe o no con anterioridad en BD
        TemperaturesHelper temp = new TemperaturesHelper();
        temp.lecturaDatosBD(ciudad, new MyCallBack() {
            @Override
            public void onCallBack(ArrayList<Bloc> value, String nombreCiudad) {
                if (value == null) {
                    //No esta en BD la ciudad, tenemos que guardarla
                    //abrimos la siguiente pantalla, pasando el nombre de la ciudad
                    new Descarregador().execute("http://api.openweathermap.org/data/2.5/forecast?q=" + nombreCiudad + "&APPID=feec37e92d4382ab121de0319ad5b368");
                }
                else {
                    //En el caso que si este, miramos si lleva en BD con una diferencia horaria de 3 horas o más

                    //Obtenemos la hora y la fecha del primer bloque de la BD
                    String horaResultado = value.get(0).getHora();
                    String[] hora = horaResultado.split(":");
                    String diaResultado = value.get(0).getDia();

                    //Obtenemos la hora actual y la fecha actual
                    DateFormat dateFormat = new SimpleDateFormat("HH");
                    Date date = new Date();
                    SimpleDateFormat diaformat = new SimpleDateFormat("yyyy-MM-dd");
                    String diaActual = diaformat.format(date);

                    //Hacemos la diferencia en valor absoluto
                    int horaPrimerBloque = Integer.parseInt(hora[0]);
                    int horaActual = Integer.parseInt(dateFormat.format(date));
                    int diferenciaHoraria = horaPrimerBloque - horaActual;

                    //Si la diferencia es de 3 horas o más o ya no estamos en el mismo dia,
                    // tenemos que actualizar la BD con los nuevos datos de esa ciudad
                    //abrimos la siguiente pantalla, pasando el nombre de la ciudad
                    if (Math.abs(diferenciaHoraria) >= 3 || !diaActual.equals(diaResultado)) {
                        new Descarregador().execute("http://api.openweathermap.org/data/2.5/forecast?q=" + nombreCiudad + "&APPID=feec37e92d4382ab121de0319ad5b368");

                    }
                    //Si no lleva mas de 3 horas, solamente tenemos que abrir la siguiente pantalla, pasando el nombre de la ciudad
                    else {
                        Intent intent = new Intent(MainActivity.this, TemperaturesHoresActivity.class);
                        intent.putExtra("nombreCiudad", nombreCiudad);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    /*
    * Clase que nos sirve para poder descargarnos los datos de la web, parsearlos y guardarlos en la BD de manera asincrona
    * */
    class Descarregador extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection client = (HttpURLConnection) url.openConnection();
                client.connect();

                InputStream is = client.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader reader = new BufferedReader(isr);

                String strCurrentLine = null;
                while ((strCurrentLine = reader.readLine()) != null) {
                    JSONObject json = new JSONObject(strCurrentLine);
                    bloquesHoras = json.getJSONArray("list");
                }

                client.disconnect();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (final JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                EditText city = findViewById(R.id.etCiutat);
                String ciudad = city.getText().toString();

                Parseador parseo = new Parseador();
                parseo.parsearDatos(bloquesHoras, ciudad);

                Intent intent = new Intent(MainActivity.this, TemperaturesHoresActivity.class);
                intent.putExtra("nombreCiudad", ciudad);
                startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
