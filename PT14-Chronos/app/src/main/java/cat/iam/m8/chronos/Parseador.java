package cat.iam.m8.chronos;

import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/*
* Clase que se encarga de parsear los datos JSON recibidos de la web
* */
public class Parseador {

    /*
    * Función que recibe los datos descargados en formato JSON y los trata de forma correcta para poder
    * añadirlos en la BD Firebase
    * */
    public void parsearDatos(JSONArray bloquesHoras, String ciudad) throws JSONException {

        String temperatura, meteo, dia, hora;
        String[] horaInici;

        ArrayList<Bloc> listaBloques = new ArrayList<Bloc>();

        //Recorremos los datos JSON
        for (int i = 0; i < bloquesHoras.length(); i++) {
            JSONObject subBloque = bloquesHoras.getJSONObject(i);

            //Partimos la fecha del JSON en dos, dia y hora cada uno por un lado
            horaInici = subBloque.getString("dt_txt").split(" ");
            dia = horaInici[0];
            hora = horaInici[1];

            //Obtenemos el objeto main
            JSONObject mainBlock = subBloque.getJSONObject("main");

            //Pasamos la temperatura de kelvins a celsius y nos quedamos solo con dos decimales
            temperatura = mainBlock.getString("temp");
            double temp = Double.parseDouble(temperatura) - 273.15;
            temp = (double)Math.round(temp * 100d) / 100d;

            //Obtenemos el array weather
            JSONArray weatherBlock = subBloque.getJSONArray("weather");
            JSONObject mainWeather = weatherBlock.getJSONObject(0);
            meteo = mainWeather.getString("main");

            //Creamos un bloque con los datos necesarios obtenidos con anterioridad y lo guardamos en la lista que guardaremos en BD
            Bloc bloque = new Bloc(dia, hora, meteo, temp);
            listaBloques.add(bloque);
        }

        //Creamos una instancia de TemperaturasHelper y guardamos la lista en BD
        TemperaturesHelper tempHelper = new TemperaturesHelper(listaBloques);
        tempHelper.guardarBBDD(ciudad);
    }
}
